<?php


namespace App\Controller;


use App\Model\User\UseCase\SignUp\Response\SignUpResponse;
use App\Services\User\Strategies\SignUp\StandartStrategy as StandartRegistration;
use App\Services\User\Strategies\SignUp\VkStrategy as VkRegistration;
use App\Services\User\Strategies\SignIn\StandartStrategy as StandartAuthorization;
use App\Services\User\UserService;
use App\Services\User\UserStrategy;
use App\Utilities\AbstractController;
use App\Utilities\Services\FrontResponse;
use Predis\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class MainController extends AbstractController {

    /**
     * Создает нового пользователя
     *
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     * @throws \Exception
     */
    public function signUpAction(Request $request, UserService $userService) {

        $registrationType   = $request->get('registration_type');

        if (!$registrationType) {
            $unknownTypeResponse = $userService->getUnknownTypeResponse()->json();
            return $unknownTypeResponse;
        }

        switch ($registrationType) {
            default:
            case $userService::STANDART_REGISTRATION:
                /** @var UserStrategy $strategy */
                $strategy = $userService->getStrategyFactory()->create(StandartRegistration::class, $request);
                break;
            case $userService::VK_REGISTRATION:
                /** @var UserStrategy $strategy */
                $strategy = $userService->getStrategyFactory()->create(VkRegistration::class, $request);
                break;
        }
        /** @var JsonResponse $result */
        $result = $userService->createUser($strategy);
        if ($result instanceof FrontResponse) {
            return $result->json();
        }

        return FrontResponse::badResponse();
    }

    /**
     * Вход в систему
     *
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function signInAction(Request $request, UserService $userService) {

        $activeUser = $userService->getActiveUser();
        if ($activeUser) {
            //TODO return redirect response
        }

        $authorizationType = $request->get('authorization_type');
        if (!$authorizationType) {
            $unknownTypeResponse = $userService->getUnknownTypeResponse()->json();
            return $unknownTypeResponse;
        }

        switch ($authorizationType) {
            case $userService::STANDART_AUTHORIZATION;
            $strategy = $userService->getStrategyFactory()->create(StandartAuthorization::class, $request);
            break;
        }

        $result = $userService->authUser($strategy);
        if ($result instanceof FrontResponse) {
            return $result->json();
        }

        return FrontResponse::badResponse();

    }

    /**
     * Удаляет пользователя
     */
    public function deleteAction() {
        return new JsonResponse('test');
    }

    /**
     * Выход из системы
     */
    public function signOutAction() {

    }

}
