<?php


namespace App\Builders\Response;


use App\Utilities\AbstractBuilder;
use App\Utilities\ResponseMessage;
use App\Utilities\Services\AbstractService;
use App\Utilities\Services\FrontResponse;

class FrontResponseBuilder extends AbstractBuilder {

    const VIOLATION = 'violation';
    const SUCCESS   = 'success';

    protected $response;

    public function getBadResponse() {
        return FrontResponse::badResponse();
    }

    public function addSuccessMessage($name, $title, $body) {
        $responseMessage = new ResponseMessage(self::SUCCESS, $name);
        $responseMessage->setTitle($title);
        $responseMessage->setBody($body);
        return $this->add('message', $responseMessage);
    }

    /**
     * @param $name
     * @param $title
     * @param $body
     * @return FrontResponseBuilder
     */
    public function addFailureMessage($name, $title, $body) {
        $responseMessage = new ResponseMessage(self::VIOLATION, $name);
        $responseMessage->setTitle($title);
        $responseMessage->setBody($body);
        return $this->add('message', $responseMessage);
    }

    protected function getProductClass() {
        return FrontResponse::class;
    }


}
