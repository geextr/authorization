<?php


namespace App\Factory;

use App\Event\Event;
use App\Utilities\InstanceFactory;
use App\Utilities\InstanceFactoryTrait;

class EventFactory implements InstanceFactory {

    use InstanceFactoryTrait;

    public static function type() {
        return Event::class;
    }

}