<?php


namespace App\Factory;


use App\Services\AbstractHelper;
use App\Utilities\InstanceFactory;
use App\Utilities\InstanceFactoryTrait;

class HelperFactory implements InstanceFactory {

    use InstanceFactoryTrait;

    public static function type() {
        return AbstractHelper::class;
    }

}