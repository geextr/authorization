<?php


namespace App\Factory;



use App\Model\AbstractCommand;
use App\Utilities\InstanceFactory;
use App\Utilities\InstanceFactoryTrait;

class CommandFactory implements InstanceFactory {

    use InstanceFactoryTrait;

    public static function type() {
        return AbstractCommand::class;
    }

}