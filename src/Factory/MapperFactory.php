<?php


namespace App\Factory;


use App\Model\AbstractMapper;
use App\Utilities\InstanceFactory;
use App\Utilities\InstanceFactoryTrait;

class MapperFactory implements InstanceFactory {

    use InstanceFactoryTrait;

    public static function type() {
        return AbstractMapper::class;
    }

}