<?php


namespace App\Services\Session;


use App\Model\User\Entity\User\User;
use App\Utilities\Services\AbstractService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SessionService extends AbstractService {

    const SESSION       = 'session';
    const SERIALIZER    = 'serializer';

    const ACTIVE_USER   = 'active_user';

    public function __construct(SessionInterface $session) {

        parent::__construct();

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, new ReflectionExtractor())];

        $serializer = new Serializer($normalizers, $encoders);

        $this->getContainer()->set(self::SESSION, $session);
        $this->getContainer()->set(self::SERIALIZER, $serializer);

    }

    public function setActiveUser(User $user) {

        $serializer = $this->getSerializer();
        $session    = $this->getSession();

        $userSerialized = $serializer->serialize($user, 'json');
        $session->set(self::ACTIVE_USER, $userSerialized);
    }

    /**
     * @return User
     */
    public function getActiveUser() {
        $activeUser = null;
        /** @var SessionInterface $session */
        $session = $this->getSession();
        $activeUserSerialized =  $session->get(self::ACTIVE_USER);
        if ($activeUserSerialized) {
            $activeUser = $this->getSerializer()->deserialize($activeUserSerialized, User::class, 'json');
        }
        return $activeUser;
    }

    /**
     * @return Serializer
     */
    protected function getSerializer() {
        return $this->getContainer()->get(self::SERIALIZER);
    }

    /**
     * @return SessionInterface
     */
    protected function getSession() {
        return $this->getContainer()->get(self::SESSION);
    }

}
