<?php

namespace App\Services\User\Strategies\SignIn;

use App\Builders\Response\FrontResponseBuilder;
use App\Factory\CommandFactory;
use App\Model\User\UseCase\SignIn\Command\SignIn;
use App\Model\User\UseCase\SignIn\Response\SignInResponse;
use App\Services\User\Helpers\PasswordHelper;
use App\Services\User\UserStrategy;
use App\Utilities\AbstractController;
use App\Utilities\Helpers\Common\TextHelper;
use App\Utilities\Model\CommandHandler;
use App\Utilities\Services\FrontResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;


class StandartStrategy extends UserStrategy {

    //используемые хелперы, сервисы и т.д.
    const COMMAND_FACTORY = 'command_factory';
    const PASSWORD_HELPER = 'password_helper';
    const TRANSLATOR      = 'translator';
    const TEXT_HELPER     = 'text_helper';

    //константы для создания ответа, который сервис возвращает в контроллер
    const ERROR_TITLE  = 'user.signin.response.error_title';
    const UNKNOWN_USER = 'user.signin.user_unknown';

    public function __construct(Request $request) {

        $commandFactory = new CommandFactory();
        $passwordHelper = new PasswordHelper();
        $textHelper     = new TextHelper();

        $translator = $this->getInvokerContainer()->get(AbstractController::TRANSLATOR);

        $this->set(self::COMMAND_FACTORY, $commandFactory);
        $this->set(self::PASSWORD_HELPER, $passwordHelper);
        $this->set(self::TRANSLATOR, $translator);
        $this->set(self::TEXT_HELPER, $textHelper);

        parent::__construct($request);

    }

    public function implementStrategy() {

        $request = $this->getRequest();

        $login       = $request->get('login');
        $password    = $request->get('password');

        $commandFactory = $this->getCommandFactory();
        /** @var TranslatorInterface $translator */
        $translator      = $this->getTranslator();

        /** @var SignIn $command */
        $command = $commandFactory->create(SignIn::class, $login);
        /** @var SignInResponse $modelResponse */
        $modelResponse = CommandHandler::handle($command, $this);
        if (!$modelResponse->getResult()) {
            //TODO возвращаем ответ о неудачной авторизации
        }

        $user = $modelResponse->getUser();
        if (!$user) {
            return $this->getUnknownUserResponse($translator, $login);
        }
        $canPass = $this->comparePasswords($password, $user->getPassword());
        if (!$canPass) {
            return $this->getUnknownUserResponse($translator, $login);
        }

        return $this->getUserSignedInResponse($login, $translator);

    }

    /**
     * @return FrontResponse
     */
    protected function getUnknownUserResponse(TranslatorInterface $translator, $login) {

        /** @var FrontResponseBuilder $responseBuilder */
        $responseBuilder = $this->getResponseBuilder();

        $name   = 'unknown_user';
        $title  = $translator->trans(self::ERROR_TITLE);
        $body   = $translator->trans(self::UNKNOWN_USER);
        $body   = $this->getTextHelper()->sprintf($body, $login);

        $response = $responseBuilder->addFailureMessage($name, $title, $body)->build();

        return $response;
    }

    /**
     * @return FrontResponse
     */
    protected function getUserSignedInResponse($login, TranslatorInterface $translator) {

        /** @var FrontResponseBuilder $responseBuilder */
        $responseBuilder = $this->getResponseBuilder();
        /** @var TextHelper $textHelper */
        $textHelper      = $this->getTextHelper();

        $name   = 'success_authorization';
        $title  = '';
        $body   = '';

        /** @var FrontResponse $response */
        $response = $responseBuilder->addSuccessMessage($name, $title, $body)->build();

        return $response;
    }

    protected function comparePasswords($password, $hash) {
        $passwordHelper = $this->getPasswordHelper();
        return $passwordHelper->compare($password, $hash);
    }

    /**
     * @return CommandFactory
     */
    protected function getCommandFactory() {
        return $this->get(self::COMMAND_FACTORY);
    }

    /**
     * @return PasswordHelper
     */
    protected function getPasswordHelper() {
        return $this->get(self::PASSWORD_HELPER);
    }

    /**
     * @return TranslatorInterface
     */
    protected function getTranslator() {
        return $this->get(self::TRANSLATOR);
    }

    /**
     * @return TextHelper
     */
    protected function getTextHelper() {
        return $this->get(self::TEXT_HELPER);
    }


}
