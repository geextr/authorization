<?php


namespace App\Services\User\Strategies\SignUp;


use App\Factory\CommandFactory;
use App\Factory\HelperFactory;
use App\Model\User\UseCase\SignUp\Response\SignUpResponse;
use App\Model\User\UseCase\SignUp\Command\SignUp;
use App\Services\User\Helpers\PasswordHelper;
use App\Services\User\Helpers\UuidHelper;
use App\Services\User\UserStrategy;
use App\Utilities\AbstractController;
use App\Utilities\Helpers\Common\TextHelper;
use App\Utilities\Model\CommandHandler;
use App\Utilities\Services\FrontResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class StandartStrategy extends UserStrategy {

    //используемые хелперы, сервисы и т.д.
    const UUID_HELPER       = 'uuid_helper';
    const TEXT_HELPER       = 'text_helper';
    const PASSWORD_HELPER   = 'password_helper';
    const COMMAND_FACTORY   = 'command_factory';
    const TRANSLATOR        = 'translator';

    //константы для создания ответа, который сервис возвращает в контроллер
    const ERROR_TITLE  = 'user.signup.response.error_title';
    const LOGIN_EXISTS = 'user.signup.user_login_exists';
    const EMAIL_EXISTS = 'user.signup.user_email_exists';

    public function __construct(Request $request) {

        $helperFactory  = new HelperFactory();
        $commandFactory = new CommandFactory();

        /** @var UuidHelper $uuidHelper */
        $uuidHelper     = $helperFactory->create(UuidHelper::class);
        /** @var PasswordHelper $passwordHelper */
        $passwordHelper = $helperFactory->create(PasswordHelper::class);
        /** @var TextHelper $textHelper */
        $textHelper     = $helperFactory->create(TextHelper::class);
        /** @var TranslatorInterface $translator */
        $translator     = $this->getInvokerContainer()->get(AbstractController::TRANSLATOR);


        $this->set(self::UUID_HELPER, $uuidHelper);
        $this->set(self::PASSWORD_HELPER, $passwordHelper);
        $this->set(self::TEXT_HELPER, $textHelper);
        $this->set(self::COMMAND_FACTORY, $commandFactory);
        $this->set(self::TRANSLATOR, $translator);

        parent::__construct($request);
    }

    public function implementStrategy() {

        /** @var Request $request */
        $request = $this->getRequest();

        /** @var UuidHelper $uuidHelper */
        $uuidHelper = $this->getUuidHelper();

        $login      = $request->get('login');
        $password   = $request->get('password');
        $email      = $request->get('email');

        $this->manageRequest($login, $password, $email);

        $uuid = $uuidHelper->generate();
        $uuid = $uuid->toString();
        $registration_date = new \DateTime();

        /** @var CommandFactory $commandFactory */
        $commandFactory = $this->get(self::COMMAND_FACTORY);
        $command = $commandFactory->create(
            SignUp::class,
            $login,
            $password,
            $email,
            $uuid,
            $registration_date
        );
        /** @var SignUpResponse $result */
        $modelResponse = CommandHandler::handle($command, $this);

        $translator = $this->getTranslator();

        if (!$modelResponse->getResult()) {
            switch ($modelResponse->getType()) {
                case SignUpResponse::LOGIN_EXISTS:
                    $response = $this->getLoginExistsResponse($login, $translator);
                    break;
                case SignUpResponse::EMAIL_EXISTS:
                    $response = $this->getEmailExistsResponse($email, $translator);
                    break;
            }
        }

        return $response;
    }

    protected function manageRequest(&$login, &$password, &$email) {

        //TODO валидация данных

        /** @var PasswordHelper $passwordHelper */
        $passwordHelper = $this->get(self::PASSWORD_HELPER);
        $password = $passwordHelper->hash($password);
    }

    protected function getLoginExistsResponse($login, TranslatorInterface $translator) {

        $responseBuilder    = $this->getResponseBuilder();
        $textHelper         = $this->getTextHelper();

        $name   = 'login';
        $title  = $translator->trans(self::ERROR_TITLE);
        $body   = $translator->trans(self::LOGIN_EXISTS);

        $body = $textHelper->sprintf($body, $login);

        $responseBuilder->addFailureMessage($name, $title, $body);
        /** @var FrontResponse $response */
        $response = $responseBuilder->build();
        return $response;
    }

    protected function getEmailExistsResponse($email, TranslatorInterface $translator) {
        $responseBuilder = $this->getResponseBuilder();
    }

    protected function getSuccessResponse(TranslatorInterface $translator) {
        $responseBuilder = $this->getResponseBuilder();
    }

    /**
     * @return UuidHelper
     */
    protected function getUuidHelper() {
        return $uuidHelper = $this->get(self::UUID_HELPER);
    }

    /**
     * @return TextHelper
     */
    protected function getTextHelper() {
        return $textHelper = $this->get(self::TEXT_HELPER);
    }

    /**
     * @return TranslatorInterface
     */
    protected function getTranslator() {
        return $translator = $this->get(self::TRANSLATOR);
    }

}
