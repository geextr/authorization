<?php


namespace App\Services\User\Helpers;


use App\Utilities\Helpers\AbstractHelper;
use App\Utilities\UuidGenerator;

class UuidHelper extends AbstractHelper {

    protected $uuidGenerator = null;

    public function generate($options = null) {

        /** @var UuidGenerator $generator */
        $generator = $this->getGenerator();
        $uuid = null;

        //по дефолту генерируется uuid4
        if (is_null($options)) {
            $uuid = $generator->generateBasic();
        }

        return $uuid;
    }

    protected function getGenerator() {
        if (is_null($this->uuidGenerator)) {
            $this->uuidGenerator = new UuidGenerator();
        }
        return $this->uuidGenerator;
    }

}
