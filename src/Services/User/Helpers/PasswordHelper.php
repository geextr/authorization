<?php


namespace App\Services\User\Helpers;



use App\Utilities\Helpers\AbstractHelper;

class PasswordHelper extends AbstractHelper {

    public function hash($password) {
        return password_hash($password, 1);
    }

    public function compare($password, $hash) {
        return password_verify($password, $hash);
    }

}
