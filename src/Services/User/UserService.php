<?php


namespace App\Services\User;

use App\Model\User\Entity\User\User;
use App\Model\User\UseCase\SignIn\Response\SignInResponse;
use App\Services\Session\SessionService;
use App\Utilities\Services\AbstractStrategyService;
use App\Utilities\Services\FrontResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserService extends AbstractStrategyService {

    const SESSSION_SERVICE          = 'session_service';

    const STANDART_REGISTRATION     = 'registration_standart';
    const VK_REGISTRATION           = 'registration_vk';

    const STANDART_AUTHORIZATION    = 'authorization_standart';

    /**
     * UserService constructor.
     * @param SessionService $sessionService
     */
    public function __construct(SessionService $sessionService) {

        parent::__construct();

        $this->getContainer()->set(self::SESSSION_SERVICE, $sessionService);

    }

    public function createUser(UserStrategy $strategy) {
        $strategy->setSessionService($this->getSessionService());
        $result = $this->manageStrategy($strategy);
        return $result;
    }

    public function authUser(UserStrategy $strategy) {
        $strategy->setSessionService($this->getSessionService());
        $result = $this->manageStrategy($strategy);
        /** @var SignInResponse $modelResponse */
        $modelResponse = $strategy->getModelResponse();
        $activeUser = $modelResponse->getUser();
        if ($activeUser) {
            $this->setActiveUser($activeUser);
        }
        return $result;
    }

    public function setActiveUser(User $user) {
        $sessionService = $this->getSessionService();
        $sessionService->setActiveUser($user);
    }

    /**
     * @return User
     */
    public function getActiveUser() {
        $sessionService = $this->getSessionService();
        $activeUser = $sessionService->getActiveUser();
        return $activeUser;
    }

    /**
     * @return FrontResponse
     */
    public function getUnknownTypeResponse() {
        //TODO сформировать ответ классом FrontResponse
        return new JsonResponse(['result' => false]);
    }

    public function getSuccessResponse() {
        //TODO сформировать ответ классом FrontResponse
        return new JsonResponse(['result' => true]);
    }

    /**
     * @return SessionService
     */
    protected function getSessionService() {
        return $this->getContainer()->get(self::SESSSION_SERVICE);
    }

}
