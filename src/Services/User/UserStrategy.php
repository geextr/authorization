<?php


namespace App\Services\User;


use App\Builders\Response\FrontResponseBuilder;
use App\Services\Session\SessionService;
use App\Utilities\Services\Strategies\ModelStrategy;
use Symfony\Component\HttpFoundation\Request;

abstract class UserStrategy extends ModelStrategy {

    public function __construct(Request $request) {
        $responseBuilder = new FrontResponseBuilder();
        $this->setResponseBuilder($responseBuilder);
        $this->setRequest($request);
    }

    public function setRequest($request) {
        $this->set('request', $request);
    }

    /**
     * @return Request
     */
    public function getRequest() {
        return $this->get('request');
    }

    public function setResponseBuilder(FrontResponseBuilder $responseBuilder) {
        $this->set('response_builder', $responseBuilder);
    }

    /**
     * @return FrontResponseBuilder
     */
    public function getResponseBuilder() {
        return $this->get('response_builder');
    }

    public function setSessionService(SessionService $sessionService) {
        $this->set('session_service', $sessionService);
    }

    /**
     * @return SessionService
     */
    public function getSessionService() {
        return $this->get('session_service');
    }

}
