<?php


namespace App\Utilities\Model;

use App\Utilities\AbstractResponse;

class ModelResponse extends AbstractResponse {

    protected $result = false;

    public function __construct(bool $result) {
        $this->result = $result;
    }

    public function getResult() {
        return $this->result;
    }

    public static function badResponse() {
        return new self(false);
    }

}
