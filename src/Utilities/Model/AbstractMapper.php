<?php


namespace App\Utilities\Model;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

abstract class AbstractMapper extends EntityRepository {

    protected $entityManager = null;
    protected $repository    = null;

    /**
     * AbstractMapper constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct($entityManager, $entityName) {
        $this->entityManager = $entityManager;
        parent::__construct($entityManager, $entityManager->getClassMetadata($entityName));
    }

    public function createEmptyObject() {
        $name = $this->getEntityName();
        if (class_exists($name)) {
            return new $name;
        }
        throw new \Exception('Couldn\'t create entity instance. Class ' . $name . ' doesn\'t exist');
    }

    public function save($entity) {
        $em = $this->getEntityManager();
        $isValid = $this->validateEntity($entity);
        if ($isValid) {
            $em->persist($entity);
            $em->flush();
            return true;
        }
        return false;
    }

    public function getEntityManager() : EntityManagerInterface {
        return $this->entityManager;
    }

    protected function validateEntity($object) {
        $entityName = $this->getEntityName();
        return $object instanceof $entityName;
    }
}
