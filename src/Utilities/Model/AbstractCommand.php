<?php


namespace App\Utilities\Model;


use App\Factory\MapperFactory;
use App\Model\User\Mapper\UserMapper;
use App\Utilities\AbstractController;
use App\Utilities\Container;
use App\Utilities\Services\Response\ResponseService;
use App\Utilities\StorageManagerTrait;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractCommand {

    private $commandArguments = null;
    private $mapperFactory    = null;

    private $translator;
    private $responseService;

    private $response = null;

    use StorageManagerTrait;

    public function __construct(...$commandArguments) {
        $this->commandArguments = $commandArguments;
        $mapperFactory = new MapperFactory();
        $this->setMapperFactory($mapperFactory);
    }


    public function execute($strategy = null) {
        if (is_null($strategy)) {
            $callback = $this->getCommand();
            if (is_callable($callback)) {
                $result = call_user_func($callback, ...$this->commandArguments);
                return $result;
            }
            return false;
        }

        $strategyMetadataClass = new \ReflectionClass($strategy);
        $command_storage_key = $strategyMetadataClass->getName();

        $callback_array = $this->getCommands();
        if (!array_key_exists($command_storage_key, $callback_array)) {
            throw new \Exception(
                'Coudn\'t create command by strategy ' . $strategy
                . '. Command is not define.');
        }
        $callback = $callback_array[$command_storage_key];
        if (is_callable($callback)) {
            $result = call_user_func($callback, ...$this->commandArguments);
            return $result;
        }
        return false;
    }

    /**
     * @param null $mapperFactory
     */
    protected function setMapperFactory($mapperFactory): void {
        $this->mapperFactory = $mapperFactory;
    }

    /**
     * @return AbstractMapper
     */
    protected function getMapperFactory() {
        return $this->mapperFactory;
    }

    protected function getEntityManager() {
        return $this->getInvokerContainer()->get(AbstractController::ENTITY_MANAGER);
    }

    /**
     * @return array
     */
    public abstract function getCommands();

    /**
     * Returns an entity for command
     *
     * @return mixed
     */
    protected abstract function getEntityName();

}
