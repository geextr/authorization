<?php


namespace App\Utilities\Model;

use App\Utilities\Services\Strategies\ModelStrategy;

class CommandHandler {

    public static function handle(AbstractCommand $command, ModelStrategy &$strategy = null) {
        $result = $command->execute($strategy);
        if (!$result instanceof ModelResponse) {
            return ModelResponse::badResponse();
        }
        $strategy->setModelResponse($result);
        return $result;
    }
}
