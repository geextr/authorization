<?php


namespace App\Utilities\Helpers\Common;


use App\Utilities\Helpers\AbstractHelper;

class TextHelper extends AbstractHelper {

    public function sprintf($string, ...$replacement) {
        return sprintf($string, ...$replacement);
    }

}
