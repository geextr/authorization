<?php


namespace App\Utilities;


abstract class AbstractBuilder {

    protected $instance = null;
    protected $buildData = [];

    protected function add($type, $value) {
        $type = ucfirst($type);
        $actionName = 'add' . $type;
        $this->buildData[$actionName] = $value;
        return $this;
    }

    protected function set($type, $value) {
        $type = ucfirst($type);
        $actionName = 'set' . $type;
        $this->buildData[$actionName] = $value;
        return $this;
    }

    public function build() {
        if (!class_exists($this->getProductClass())) {
            throw new \Exception('Product class' . $this->getProductClass() . ' doesn\'t exist');
        }
        $productClass = $this->getProductClass();
        $instance = new $productClass();
        foreach ($this->buildData as $actionName => $injectedValue) {
            if (method_exists($instance, $actionName)) {
                $instance->$actionName($injectedValue);
                continue;
            }
            throw new \Exception('Can\'t build an instance for ' . $instance. '. Provided action ' . $actionName . ' doesn\'t exist.');
        }
        return $instance;
    }

    abstract protected function getProductClass();

}
