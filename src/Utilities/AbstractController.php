<?php


namespace App\Utilities;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as ParentAbstractController;

class AbstractController extends ParentAbstractController {

    use StorageManagerTrait;

    const TRANSLATOR        = 'translator';
    const RESPONSE_SERVICE  = 'response_service';
    const ENTITY_MANAGER    = 'entity_manager';

    public function __construct(TranslatorInterface $translator, EntityManagerInterface $em) {
        $this->getContainer()->set(self::TRANSLATOR, $translator);
        $this->getContainer()->set(self::ENTITY_MANAGER, $em);
    }



}
