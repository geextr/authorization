<?php


namespace App\Utilities;

class Container {

    protected           $container    = [];
    protected static    $instances    = [];

    private function __construct() {}

    public static function getInstance($type) {
        if (!array_key_exists($type, self::$instances)) {
            self::$instances[$type] = new self();
        }
        return self::$instances[$type];
    }

    public function set($key, $value) {
        $this->container[$key] = $value;
    }

    public function get($key) {
        return (array_key_exists($key, $this->container)) ? ($this->container[$key]) : false;
    }

    public function push($key, $value) {
        if (array_key_exists($key, $this->container)) {
            $this->container[$key][] = $value;
            return;
        }
        $this->container[$key] = [$value];
    }

}
