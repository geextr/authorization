<?php


namespace App\Utilities\Services;


use App\Utilities\Factory\StrategyFactory;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractStrategyService extends AbstractService {

    private $strategyFactory = null;

    private $strategy_instances = [];

    protected function manageStrategy(&$strategy){
        if (!($strategy instanceof AbstractStrategy)) {
            throw new \Exception('Object to manage has to be an instance of AbstractStrategy');
        }

        $result = $strategy->implementStrategy();

        return $result;
    }

    /**
     * @return StrategyFactory
     */
    public function getStrategyFactory() {
        if (is_null($this->strategyFactory)) {
            $this->strategyFactory = new StrategyFactory();
        }
        return $this->strategyFactory;
    }



}
