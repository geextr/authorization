<?php


namespace App\Utilities\Services;


use App\Utilities\AbstractResponse;
use App\Utilities\ResponseMessage;
use App\Utilities\StorageManagerTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

class FrontResponse extends AbstractResponse {

    use StorageManagerTrait;

    private $content = null;

    private $container = null;

    protected $result = false;

    private const MESSAGES_RESPONSE_KEY   = 'messages';
    private const VIOLATIONS              = 'violations';
    private const SUCCESS_MESSAGES        = 'success_messages';

    private const BAD_RESPONSE_CONTENT      = 'Unknown error';

    public static function badResponse() {
        return new JsonResponse([
            ResponseMessage::VIOLATION => [
                self::BAD_RESPONSE_CONTENT
            ]
        ]);
    }

    public function addMessage(ResponseMessage $message) {
        $this->getContainer()->push(self::MESSAGES_RESPONSE_KEY, $message);
        return $this;
    }

    public function getMessages() {
        return $this->getContainer()->get(self::MESSAGES_RESPONSE_KEY);
    }


    public function json() {
        $formattedResponse = $this->format();
        return new JsonResponse($formattedResponse);
    }

    protected function format() {

        $formattedResponse = [];

        $messages = $this->getMessages();
        if ($messages) {
            /** @var ResponseMessage $message */
            foreach ($messages as $message) {
                $type = $message->getType();
                $formattedMessage = $message->toArray();
                switch ($type) {
                    case ResponseMessage::VIOLATION:
                        $formattedResponse[self::VIOLATIONS][] = $formattedMessage;
                        break;
                    case ResponseMessage::SUCCESS:
                        $formattedResponse[self::SUCCESS_MESSAGES][] = $formattedMessage;
                        break;
                }

            }

            return $formattedResponse;
        }

    }

}
