<?php


namespace App\Utilities\Services;


use App\Event\EventManager;
use App\Utilities\Container;

abstract class AbstractService {

    private const EVENT_MANAGER = 'event_manager';

    private $serviceName;

    public function __construct() {
        $reflection = new \ReflectionClass($this);
        $this->serviceName = $serviceName = $reflection->getNamespaceName();
        /** @var Container $container */
        $container = Container::getInstance($serviceName);
        $container->set(self::EVENT_MANAGER, new EventManager());

    }

    /**
     * @return EventManager
     */
    protected function getEventManager() {
        return $this->getContainer()->get(self::EVENT_MANAGER);
    }

    /**
     * @return Container
     */
    protected function getContainer() {
        return Container::getInstance($this->serviceName);
    }

}
