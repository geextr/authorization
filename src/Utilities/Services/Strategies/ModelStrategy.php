<?php


namespace App\Utilities\Services\Strategies;


use App\Utilities\Model\ModelResponse;
use App\Utilities\Services\AbstractStrategy;

abstract class ModelStrategy extends AbstractStrategy {

    const MODEL_RESPONSE = 'model_response';

    public function setModelResponse(ModelResponse $response) {
        $this->set(self::MODEL_RESPONSE, $response);
    }

    public function getModelResponse() {
        return $this->get(self::MODEL_RESPONSE);
    }

}
