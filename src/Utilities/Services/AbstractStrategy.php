<?php


namespace App\Utilities\Services;


use App\Utilities\AbstractController;
use App\Utilities\Container;
use App\Utilities\StorageManagerTrait;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractStrategy {

    const STRATEGY_CONTAINER = 'container.strategy';

    use StorageManagerTrait;

    public function set($key, $value) {
        Container::getInstance(self::STRATEGY_CONTAINER)->set($key, $value);
    }

    public function get($key) {
        return Container::getInstance(self::STRATEGY_CONTAINER)->get($key);
    }

    protected function getEntityManager() {
        return $this->getInvokerContainer()->get(AbstractController::ENTITY_MANAGER);
    }



    public abstract function implementStrategy();

}
