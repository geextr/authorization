<?php


namespace App\Utilities;


trait InstanceFactoryTrait {

    protected $instance = null;

    public function create($class, ...$arguments) {
        if (is_null($this->instance) || !($this->instance instanceof $class)) {
            $classMetadata = new \ReflectionClass($class);
            if (!method_exists(self::class, 'type')) {
                throw new \Exception('Factory has to be an instance of InstanceFactory');
            }
            $factoryType = self::type();

            $parentClass = $classMetadata->getParentClass();

            if (!$parentClass) {
                throw new \Exception('Object has to be an instance of ' . $factoryType);
            }
            $parentTypeNamespace = $parentType = $parentClass->getName();

            $this->prepare($parentType, $factoryType);

            if ($factoryType != $parentType && !is_subclass_of($class, $parentTypeNamespace)) {
                $createObject = $classMetadata->getNamespaceName();
                throw new \Exception('Can\'t create an instance of ' . $createObject
                    . '. Wrong factory type.');
            }
            if (class_exists($class)) {
                if ($arguments) {
                    $this->instance = new $class(...$arguments);
                } else {
                    $this->instance = new $class;
                }

            } else {
                throw new \Exception('Class ' . $class . ' doesn\'t exist in scope');
            }
        }

        return $this->instance;
    }

    protected function prepare(&$createObjectType, &$factoryType) {
       $this->canonicalize($createObjectType);
       $this->canonicalize($factoryType);
    }

    protected function canonicalize(&$name) {
        $name = strtolower(stripcslashes($name));
    }

}