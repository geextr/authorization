<?php


namespace App\Utilities\Factory;


use App\Utilities\Services\AbstractStrategy;
use App\Utilities\InstanceFactory;
use App\Utilities\InstanceFactoryTrait;

class StrategyFactory implements InstanceFactory {

    use InstanceFactoryTrait;

    public static function type() {
        return AbstractStrategy::class;
    }

}
