<?php


namespace App\Utilities;


use PHPUnit\Runner\Exception;

class ResponseMessage {

    protected $name;
    protected $title;
    protected $body;

    protected $type;

    const VIOLATION = 'violation';
    const SUCCESS = 'success';

    protected $typeConfig = [
        self::VIOLATION,
        self::SUCCESS
    ];

    public function __construct($type, $name) {
        if (!in_array($type, $this->typeConfig)) {
            throw new Exception('Unknown response message type.');
        }
        $this->type = $type;
        $this->name = $name;
    }

    /**
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param string $body
     */
    public function setBody($body) {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

    public function toArray() {
        $formattedMessage = [];
        $formattedMessage[$this->getType()][$this->getName()] = [
            'title' => $this->getTitle(),
            'body'  => $this->getBody()
        ];
        return $formattedMessage;
    }



}
