<?php


namespace App\Utilities;


use Ramsey\Uuid\Uuid;

class UuidGenerator {

    /**
     * Пока генерируем uuid4,
     * впоследствие можно усложнять
     *
     * @return \Ramsey\Uuid\UuidInterface
     */
    public function generateBasic() {
        return Uuid::uuid4();
    }

}