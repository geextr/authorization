<?php


namespace App\Utilities;


trait StorageManagerTrait {

    /**
     * @return Container
     */
    protected function getContainer() {
        return Container::getInstance(self::class);
    }

    /**
     * @return Container
     */
    protected function getInvokerContainer() {
        return Container::getInstance(AbstractController::class);
    }

}
