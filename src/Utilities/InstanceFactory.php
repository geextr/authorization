<?php


namespace App\Utilities;


interface InstanceFactory
{

    public function create($class, ...$arguments);

    public static function type();

}