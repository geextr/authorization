<?php


namespace App\Model\User\Mapper;


use App\Model\User\Entity\User\User;
use App\Utilities\Model\AbstractMapper;

class UserMapper extends AbstractMapper {

    public function __construct($entityManager)
    {
        parent::__construct($entityManager, User::class);
    }

    public function findByUuid($uuid) {
        /** @var User $user */
        $user = $this->findOneBy(['uuid' => $uuid]);
        return $user;
    }

    public function findByEmail($email) {
        $user = $this->findOneBy(['email' => $email]);
        return $user;
    }

    public function findByLogin($login) {
        $user = $this->findOneBy(['login' => $login]);
        return $user;
    }


}
