<?php


namespace App\Model\User\UseCase\SignUp\Command;


use App\Factory\MapperFactory;
use App\Model\User\Entity\User\User;
use App\Model\User\Mapper\UserMapper;
use App\Model\User\UseCase\SignUp\Response\SignUpResponse;
use App\Services\User\Strategies\SignUp\StandartStrategy;
use App\Services\User\Strategies\SignUp\VkStrategy;
use App\Utilities\Model\AbstractCommand;
use App\Utilities\StorageManagerTrait;
use Doctrine\ORM\EntityManagerInterface;

class SignUp extends AbstractCommand {

    use StorageManagerTrait;

    private $container = null;

    public function getCommand() {
        return false;
    }

    public function getCommands() {
        return [
            StandartStrategy::class => function ($login, $password, $email, $uuid, $registration_date) {

                /** @var MapperFactory $mapperFactory */
                $mapperFactory = $this->getMapperFactory();
                /** @var EntityManagerInterface $em */
                $em = $this->getEntityManager();
                /** @var UserMapper $userMapper */
                $userMapper = $mapperFactory->create(UserMapper::class, $em);

                /** @var User $entity */
                $entity = $userMapper->createEmptyObject();

                $entity->setLogin($login);
                $entity->setPassword($password);
                $entity->setEmail($email);
                $entity->setUuid($uuid);
                $entity->setRegistrationDate($registration_date);

                $userByLogin = $userMapper->findByLogin($login);
                if ($userByLogin) {
                    return new SignUpResponse(false, SignUpResponse::LOGIN_EXISTS);
                }

                $userByEmail = $userMapper->findByEmail($email);
                if ($userByEmail) {
                    return new SignUpResponse(false, SignUpResponse::EMAIL_EXISTS);
                }

                $result =  $userMapper->save($entity);
                if ($result) {
                    return new SignUpResponse(true);
                }
            },
            VkStrategy::class => function ($token) {

            }
        ];
    }

    protected function getEntityName() {
        return User::class;
    }

}
