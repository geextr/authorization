<?php


namespace App\Model\User\UseCase\SignUp\Response;


use App\Utilities\Model\ModelResponse;

class SignUpResponse extends ModelResponse {

    const LOGIN_EXISTS = 'login_exists';
    const EMAIL_EXISTS = 'email_exists';

    protected $type;

    public function __construct(bool $result, $type = null) {
        $this->setType($type);
        parent::__construct($result);
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

}
