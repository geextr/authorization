<?php


namespace App\Model\User\UseCase\SignIn\Response;


use App\Model\User\Entity\User\User;
use App\Utilities\Model\ModelResponse;

class SignInResponse extends ModelResponse {

    const LOGIN_EXISTS = 'login_exists';
    const EMAIL_EXISTS = 'email_exists';

    protected $type;
    protected $user;

    public function __construct(bool $result, $object = null) {
        if ($object) {
            $this->setUser($object);
        }
        parent::__construct($result);
    }


    public function setUser(User $user) {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

}
