<?php


namespace App\Model\User\UseCase\SignIn\Command;


use App\Factory\MapperFactory;
use App\Model\User\Entity\User\User;
use App\Model\User\Mapper\UserMapper;
use App\Model\User\UseCase\SignIn\Response\SignInResponse;
use App\Services\User\Strategies\SignIn\StandartStrategy;
use App\Utilities\Model\AbstractCommand;

class SignIn extends AbstractCommand {

    public function getCommands() {
        return [
            StandartStrategy::class => function($login) {
                /** @var MapperFactory $mapperFactory */
                $mapperFactory = $this->getMapperFactory();
                /** @var UserMapper $userMapper */
                $userMapper = $mapperFactory->create(UserMapper::class, $this->getEntityManager());
                $user = $userMapper->findByLogin($login);
                if (!$user) {
                    return new SignInResponse(false);
                }
                return new SignInResponse(true, $user);
            }
        ];
    }

    protected function getEntityName() {
        return User::class;
    }


}
