<?php


namespace App\Tests\Unit\Model\User\Entity\User\SignUp;

use App\Model\User\Entity\User\User;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class RequestTest extends TestCase {

    public function testSuccess(): void {
        $user = new User(
            $id = 1,
            $login = 'testuser',
            $email = 'test@mail.com',
            $password = password_hash('password', PASSWORD_ARGON2I),
            $uuid = Uuid::uuid4()->toString(),
            $registrationDate = new \DateTime()
        );
        self::assertEquals($id, $user->getId());
        self::assertEquals($login, $user->getLogin());
        self::assertEquals($email, $user->getEmail());
        self::assertEquals($password, $user->getPassword());
        self::assertEquals($uuid, $user->getUuid());
        self::assertEquals($registrationDate, $user->getRegistrationDate());
    }

}